package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.List;
import java.lang.Math;


/**
 * A simple disambiguation model for TRmorph output.
 
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */

public class Disambiguator
{
    private final JSONObject model;
    private final long ntypes;
    private final long ntokens;
    private final long ratypes;

    public Disambiguator(Reader in) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        model = (JSONObject)((JSONArray)parser.parse(in)).get(1);
        ntokens = (Long) model.get("##tokens");
        ntypes = model.keySet().size();
        ratypes = (Long) model.get("##ratypes");
    }

    public String disambiguate(List<String> analyses) {
        double bestScore = Double.NEGATIVE_INFINITY;
        String bestAnalysis = null;
        for (String a: analyses) {
            double score = scoreAnalysis(a);
            if (score > bestScore) {
                bestScore = score;
                bestAnalysis = a;
            }
        }

        return bestAnalysis;
    }

    private double scoreAnalysis(String a)
    {
        String root = a.substring(0, a.indexOf('<') - 1);
        String astring = a.substring(a.indexOf('<'), a.length());
        double score = 0;
        if (model.containsKey(astring)) {
            JSONObject rdict = (JSONObject) model.get(astring);
            long tokenCountA = (Long) rdict.get("##tokens");
            long typeCountA = rdict.keySet().size();
            long tokenCountRA = 0;
            if (rdict.containsKey(root)) {
                tokenCountRA = (Long) rdict.get(root);
            }
            double logProbA = Math.log(tokenCountA + 1) - Math.log(ntokens + ntypes);
            double logProbRA = Math.log(tokenCountRA + 1) - Math.log(tokenCountA + typeCountA);
            score = logProbA + logProbRA;
        } else {
            double logProbA = - Math.log(ntokens + ntypes);
            double logProbRA = Math.log(ratypes) - Math.log(ntokens);
            score = logProbA + logProbRA;
        }
        return score;
    }
/*
    private double scoreAnalysis(String a)
    {
//        List<InflectionalGroup> igs = TrMorph.splitAnalysis(a);
        double score = 0;
        for (InflectionalGroup ig: igs) {
            String A = ig.pos + ig.inflections;
            if (model.containsKey(A)) {
                JSONObject rdict = (JSONObject) model.get(A);
                long tokenCountA = (Long) rdict.get("##tokens");
                long typeCountA = rdict.keySet().size();
                long tokenCountRA = 0;
                if (rdict.containsKey(ig.root)) {
                    tokenCountRA = (Long) rdict.get(ig.root);
                }
                double logProbA = Math.log(tokenCountA + 1) - Math.log(ntokens + ntypes);
                double logProbRA = Math.log(tokenCountRA + 1) - Math.log(tokenCountA + typeCountA);
                score += logProbA + logProbRA;
            } else {
                double logProbA = - Math.log(ntokens + ntypes);
                double logProbRA = Math.log(ratypes) - Math.log(ntokens);
                score += logProbA + logProbRA;
            }
        }
        return score;
    }
 */

}
