package de.uni.tuebingen.sfs.clarind.trmorphology.trmorph;

import java.io.File;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;

import java.util.*;


/**
 * This class implements a simple FST decoder. 
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */
public class Fst 
{
    ArrayList<FstNode> nodes = null;
    TreeSet<String> lexicon  = null;

    public Fst()
    {
        lexicon = new TreeSet<String>(new FstLexComp());
        lexicon.add(""); // epsilon
        lexicon.add("@_UNKNOWN_SYMBOL_@");
        lexicon.add("@_IDENTITY_SYMBOL_@");
        nodes = new ArrayList<FstNode>();
        FstNode start = new FstNode();
        nodes.add(start);
    }

    public Fst(String filename) throws FileNotFoundException
    {
        this();
        readATT(filename);
    }

    public Fst(InputStream is)
    {
        this();
        readATT(is);
    }

    public void readATT(String filename) throws FileNotFoundException
    {
        readATT(new FileInputStream(filename));
    }

    public void readATT(InputStream is)
    {
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(is, "UTF8"));
                

            String line;
            int linenum = 0;
            while ((line = in.readLine()) != null) {
                ++linenum;
                if (line.trim().length() == 0) continue;
                String [] fields = line.split("\t");
                switch (fields.length) {
                    case 5: // ignore the cost
                    case 4: // an arc description
                        if ("@0@".equals(fields[2])) fields[2] = "";
                        if ("@0@".equals(fields[3])) fields[3] = "";
                        lexicon.add(fields[2]);
                        lexicon.add(fields[3]);
                        addArc(Integer.parseInt(fields[0]),
                               fields[3],
                               fields[2],
                               Integer.parseInt(fields[1]));
                        break;
                    case 2:
                    case 1:
                        nodes.get(Integer.parseInt(fields[0])).setFinal();
                        break;
                    default:
                        System.out.format("! %d: wrong number of fileds: `%s'\n", 
                                linenum, line);
                        break;
                }
            }
            in.close();
        } catch (Exception e) {
            System.err.println("Cannot read ATT file\n" + e);
            e.printStackTrace();
        }
    }

    public void writeLexicon()
    {
        for (String li: lexicon) {
                System.out.println(li + ": " + li.length());
        }
    }

    public void writeATT()
    {
        for(int i = 0; i < nodes.size(); i++) {
            nodes.get(i).writeATT(i);
            if (nodes.get(i).isFinal()) {
                System.out.println(i);
            }
        }
    }

    public void addNode(int node)
    {
        if (node < nodes.size()) {
            if (nodes.get(node) == null) {
                nodes.set(node, new FstNode());
            }
        } else {
            for (int i = nodes.size(); i <= node; i++) {
                nodes.add(null);
            }
            nodes.set(node, new FstNode());
        }
    }

    public void addArc(int start, String inSym, String outSym, int end)
    {
        addNode(start);
        addNode(end);
        lexicon.add(inSym);
        lexicon.add(outSym);
        FstNode n = nodes.get(start);
        n.addArc(inSym, outSym, end);
    }


    public String [] tokenize(String s)
    {
        ArrayList<String> tokens = new ArrayList<String>();
        int start = 0;
        while (start < s.length()) {
            boolean found = false;
            for (String li: lexicon) {
                if (li.length() == 1)
                    break;
                if ((li.length() + start) > s.length()) 
                    continue;
                if (li.equals(s.substring(start, start + li.length()))) {
                    tokens.add(li);
                    found = true;
                    start = start + li.length();
                    break;
                }
            }
            if (!found) {
                tokens.add(s.substring(start, start + 1));
                start += 1;
            }
        }
        String [] ret = tokens.toArray(new String[tokens.size()]);
        return ret;
    }

    private class ApplyState
    {
        int node = 0;
        int inputPosition = 0;
        String output = "";
    }

    private ApplyState expandAgenda(ApplyState curState,
            String [] tokens, Stack<ApplyState> stack)
    {
        String in = null;
        FstArc [] arcs = null;
        if (curState.inputPosition < tokens.length) {
            in = tokens[curState.inputPosition];
            arcs = nodes.get(curState.node).findArcs(in, lexicon.contains(in));
        } else {
            in = null;
            arcs = nodes.get(curState.node).findArcs(null, false);
        }

        if (arcs == null) return null;

        ApplyState newState = null;
        for (int i = arcs.length - 1; i >= 0; i--) {
            newState = new ApplyState();
            newState.node = arcs[i].nextNode;

            newState.output = curState.output;
            newState.inputPosition = curState.inputPosition;
            if (! "".equals(arcs[i].inSym)) {
                newState.inputPosition += 1;
            }

            if (! "".equals(arcs[i].outSym)) {
                if ("@_IDENTITY_SYMBOL_@".equals(arcs[i].outSym)) {
                    newState.output += in;
                } else {
                    newState.output += arcs[i].outSym;
                }
            }

            if (i != 0) {
                stack.push(newState);
            }
        }
        return newState;
    }

    private ApplyState expandAgenda(String [] tokens, Stack<ApplyState> stack)
    {
        return expandAgenda(new ApplyState(), tokens, stack);
    }


    public List<String> apply(String [] tokens)
    {
        ArrayList<String> result = new ArrayList<String>();
        Stack<ApplyState> stack = new Stack<ApplyState>();

        ApplyState cur = expandAgenda(tokens, stack);

        while (cur != null) {
            if (cur.inputPosition == tokens.length) {
                if (nodes.get(cur.node).isFinal()) {
                    result.add(cur.output);
                } else {
                }
            } 
            cur = expandAgenda(cur, tokens, stack);
            if (cur == null) {
                try {
                    cur = stack.pop();
                } catch (EmptyStackException e) {
                    cur = null;
                }
            }
        }
        return result;
    }

    public List<String> apply(String s)
    {
        return apply(tokenize(s));
    }

    static class FstLexComp implements Comparator<String>{
        @Override
        public int compare(String str1, String str2) {
            if (str1.length() == str2.length()) {
                return - str1.compareTo(str2);
            } else if (str1.length() > str2.length()) {
                return -1;
            } else {
                return 1;
            }
        }
                 
    }
}
