package de.uni.tuebingen.sfs.clarind.trmorphology.core;

import de.uni.tuebingen.sfs.clarind.trmorphology.trmorph.TrMorph;
import de.uni.tuebingen.sfs.clarind.trmorphology.trmorph.Disambiguator;
import eu.clarin.weblicht.wlfxb.api.TextCorpusProcessor;
import eu.clarin.weblicht.wlfxb.api.TextCorpusProcessorException;
import eu.clarin.weblicht.wlfxb.tc.api.*;
import eu.clarin.weblicht.wlfxb.tc.xb.FeatureStored;
import eu.clarin.weblicht.wlfxb.tc.xb.TextCorpusLayerTag;
import opennlp.tools.sentdetect.SentenceDetector;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

import java.io.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Turkish morphology web service.
 *
 * @author Çağrı Çöltekin {@literal <cagri@coltekin.net>}
 */

public class TrMorphologyTool implements TextCorpusProcessor {

    private static final EnumSet<TextCorpusLayerTag> 
        requiredLayers = EnumSet.of(TextCorpusLayerTag.TEXT);
    public static final String TOKENIZER_MODEL = "/models/tr-token.bin";
    public static final String SENTSPLITTER_MODEL = "/models/tr-sent.bin";
    public static final String MORPH_MODEL = "/models/trmorph.att";
    public static final String GUESSER_MODEL = "/models/trmorph-guesser.att";
    public static final String DISAMBIGUATION_MODEL = "/models/disambiguation.json";
    private final SentenceDetector sentSplitter;
    private final Tokenizer tokenizer;
    private final TrMorph analyzer;
    private final TrMorph guesser;
    private final Disambiguator disambiguator;

    public TrMorphologyTool()  throws TextCorpusProcessorException {

        InputStream inp = null;
        // read in the sentence splitter model
        try {
            inp = this.getClass().getResourceAsStream(SENTSPLITTER_MODEL);
            sentSplitter = new SentenceDetectorME(new SentenceModel(inp));
        } catch (Exception ex) {
            Logger.getLogger(TrMorphologyTool.class.getName())
                .log(Level.SEVERE, null, ex);
            throw new
                TextCorpusProcessorException("Failed reading the model: " + SENTSPLITTER_MODEL, ex);
        } finally {
            try {
                if (inp != null) {
                    inp.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TrMorphologyTool.class.getName())
                    .log(Level.SEVERE, null, ex);
            }
        }

        // read in the tokenizer model
        try {
            inp = this.getClass().getResourceAsStream(TOKENIZER_MODEL);
            tokenizer = new TokenizerME(new TokenizerModel(inp));
        } catch (Exception ex) {
            Logger.getLogger(TrMorphologyTool.class.getName())
                .log(Level.SEVERE, null, ex);
            throw new 
                TextCorpusProcessorException("Failed reading the model"+ TOKENIZER_MODEL, ex);
        } finally {
            try {
                if (inp != null) {
                    inp.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TrMorphologyTool.class.getName())
                    .log(Level.SEVERE, null, ex);
            }
        }

        // initialize the morphological analyzer model
        try {
            inp = this.getClass().getResourceAsStream(MORPH_MODEL);
            analyzer = new TrMorph(inp);
        } catch (Exception ex) {
            Logger.getLogger(TrMorphologyTool.class.getName())
                    .log(Level.SEVERE, null, ex);
            throw new
                    TextCorpusProcessorException("Failed reading the model"+ MORPH_MODEL, ex);
        } finally {
            try {
                if (inp != null) {
                    inp.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TrMorphologyTool.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }

        // ... and the guesser
        try {
            inp = this.getClass().getResourceAsStream(GUESSER_MODEL);
            guesser = new TrMorph(inp);
        } catch (Exception ex) {
            Logger.getLogger(TrMorphologyTool.class.getName())
                    .log(Level.SEVERE, null, ex);
            throw new
                    TextCorpusProcessorException("Failed reading the model"+ GUESSER_MODEL, ex);
        } finally {
            try {
                if (inp != null) {
                    inp.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TrMorphologyTool.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }

        // initialize the disambiguation model
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(
                     this.getClass().getResourceAsStream(DISAMBIGUATION_MODEL)));
            disambiguator = new Disambiguator(reader);
        } catch (Exception ex) {
            Logger.getLogger(TrMorphologyTool.class.getName())
                    .log(Level.SEVERE, null, ex);
            throw new
                    TextCorpusProcessorException("Failed reading the model"
                            + DISAMBIGUATION_MODEL, ex);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(TrMorphologyTool.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
        }
    }


    @Override
    public EnumSet<TextCorpusLayerTag> getRequiredLayers() {
        return requiredLayers;
    }

    @Override
    public void process(TextCorpus textCorpus) 
        throws TextCorpusProcessorException {
        String text = textCorpus.getTextLayer().getText();


        TokensLayer tokLayer = textCorpus.createTokensLayer();
        SentencesLayer sentLayer = textCorpus.createSentencesLayer();
        PosTagsLayer posTags = textCorpus.createPosTagsLayer("metu-sabanci");
        MorphologyLayer morphLayer = textCorpus.createMorphologyLayer();
        LemmasLayer lemmaLayer = textCorpus.createLemmasLayer();

        String sentences[] = sentSplitter.sentDetect(text);

        List<Token> tcfTokens = new ArrayList<Token>();
        for (int i = 0; i < sentences.length; i++) {
            String tokens[] = tokenizer.tokenize(sentences[i]);
            for (int j = 0; j < tokens.length; j++) {
                List<String> analyses = analyzer.apply(tokens[j]);
                // guess if token is not analyzed
                if (analyses == null || analyses.isEmpty()) {
                    analyses = guesser.apply(tokens[j]);
                }
                // if guesser fails, assume it is a noun
                if (analyses == null || analyses.isEmpty()) {
                    analyses = new ArrayList<String>();
                    analyses.add(tokens[j] + "<N>");
                }
                String bestA = disambiguator.disambiguate(analyses);
                List<Map<String,String>> features = analyzer.features(bestA);

                for (int k = 0; k < features.size(); k++) {
                    Map<String,String> ig = features.get(k);
                    String tokenForm = (k == features.size() - 1) ? tokens[j] : "_";
                    Token token = tokLayer.addToken(tokenForm);
                    tcfTokens.add(token);
                    posTags.addTag(ig.get("Pos"), token);
                    lemmaLayer.addLemma(ig.get("Lemma"), token);

                    List<Feature> tcfFeats = new ArrayList<Feature>();
                    for(String label: ig.keySet()) {
//                        if (label.equals("Pos") || label.equals("Lemma")) {
//                            continue;
//                        }
                        Feature f = morphLayer.createFeature(label, ig.get(label));
                        tcfFeats.add(f);
                    }
                    morphLayer.addAnalysis(token, tcfFeats);
                }

//                Token token = tokLayer.addToken(tokens[j]);
            }
            sentLayer.addSentence(tcfTokens);
        }
    }

}

